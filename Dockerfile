from python:3.9


RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    git \
    git-lfs \
    tree \
    fakeroot

# set shell to bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

ENV TERM xterm-256color
ENV LANG=C.UTF-8

RUN pip3 install \
    anybadge \
    cryptography==2.8 \
    fixtopt-xtofl \
    inotify_simple \
    junit2html \
    kconfiglib \
    mypy==0.910 \
    paramiko \
    pdoc3 \
    pep8 \
    pycodestyle \
    pyexpect \
    pyfiglet==0.7 \
    pylint \
    pySerial \
    pytest \
    pytest-cov \
    pytest-dependency \
    pytest-html \
    python-gitlab==3.15.0 \
    python-jenkins \
    pytk \
    pyyaml \
    radon \
    requests \
    robotframework \
    scapy \
    scp \
    setuptools \
    twine \
    xenon

